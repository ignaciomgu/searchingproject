/**
 Se crea una instancia de Modal
 */
Vue.component('modal', {
    template: '#modal-template'
});
/**
Se crea una instancia de VueJs 
 */
const vm = new Vue ({
  el: '#vue-instance',
  data () {
    return {
      urlApiBackEnd: 'http://127.0.0.1:3000', // Url del Backend que se comunica con el Elastic
      textToSearch: '', // Texto a buscar, por defecto vacio
      timeOutToWriteTheTextToSearch: null, // Timeout para escribir el texto a buscar
      listOfBooksFound: [], // Muestra los resultados de la búsqueda, es decir, la lista de libros de respuesta
      numberOfMatches: null, // Total de coincidencias encontradas en la búsqueda
      searchOffset: 0, // Offset de Paginación
      seen: true, // Variable para visualizacion de componentes en el html
      showModal: true,
        selectedParagraph: null, // Selected paragraph object
        bookOffset: 0, // Offset for book paragraphs being displayed
        paragraphs: [] // Paragraphs being displayed in book preview window
    }
  },
  // Esta porción de código se ejecuta al iniciar este archivo JS
  async created () {
    this.listOfBooksFound = await this.search() // Se realiza una búsqueda del resultado por defecto
  },
  methods: {
    /** Este método se ejecuta luego de un time out en milisegundos de escribir un texto a buscar*/
    onSearchInput () {
      clearTimeout(this.timeOutToWriteTheTextToSearch);
      this.searchDebounce = setTimeout(async () => {
        this.searchOffset = 0;
        this.listOfBooksFound = await this.search()
      }, 100)
    },
    /** Llama a la API del Backend y envía el texto a buscar y el offset de paginación*/
    async search () {
      if(this.textToSearch === ""){
          this.numberOfMatches = 0;
          this.seen = false;
          return;
      }else{
          this.seen = true;
      }
      const response = await axios.get(`${this.urlApiBackEnd}/buscar`, { params: { textToSearch: this.textToSearch, offset: this.searchOffset } });
      this.numberOfMatches = response.data.hits.total;
      return response.data.hits.hits;
    },
    /** Llama a la API del Backend y envía el texto a buscar y el offset de paginación para llamar a la siguiente pagina de coincidencias*/
    async nextPage () {
      if (this.numberOfMatches > 10) {
        this.searchOffset += 10;
        if (this.searchOffset + 10 > this.numberOfMatches) { this.searchOffset = this.numberOfMatches - 10}
        this.listOfBooksFound = await this.search();
        document.documentElement.scrollTop = 0
      }
    },
    /** Llama a la API del Backend y envía el texto a buscar y el offset de paginación para llamar a la pagina anterior de coincidencias*/
    async prevPage () {
      this.searchOffset -= 10;
      if (this.searchOffset < 0) { this.searchOffset = 0 }
      this.listOfBooksFound = await this.search();
      document.documentElement.scrollTop = 0
    },
      /** Llama a la API del Backend y envía los datos de la coincidencia (Título del libro mas offset de pagina) para obtener la página en donde se encuentra la misma y mostrarla*/
      async getParagraphs (bookTitle, offset) {
          try {
              this.bookOffset = offset;
              const start = this.bookOffset < 0 ? 0 : this.bookOffset ;
              const end = this.bookOffset + 10;
              const response = await axios.get(`${this.urlApiBackEnd}/obtenerParrafo`, { params: { bookTitle, start, end } });
              return response.data.hits.hits;
          } catch (err) {
              console.error(err)
          }
      },
      /** Pide la siguiente página del libro */
      async nextBookPage () {
          this.$refs.bookModal.scrollTop = 0;
          this.paragraphs = await this.getParagraphs(this.selectedParagraph._source.title, this.bookOffset + 10); // próximos 10 párrafos
      },
      /** Pide la página anterior del libro */
      async prevBookPage () {
          this.$refs.bookModal.scrollTop = 0;
          this.paragraphs = await this.getParagraphs(this.selectedParagraph._source.title, this.bookOffset - 10); // anteriores 10 párrafos
      },
      /** Muestra los párrafos pedidos en el modal de visualización de libros */
      async showBookModal (searchHit) {
          try {
              document.body.style.overflow = 'hidden';
              this.selectedParagraph = searchHit;
              this.paragraphs = await this.getParagraphs(searchHit._source.title, searchHit._source.location - 5);
          } catch (err) {
              console.error(err)
          }
      },
      /** Cierra modal de visualización de libros */
      closeBookModal () {
          document.body.style.overflow = 'auto';
          this.selectedParagraph = null;
      }
  }
});
