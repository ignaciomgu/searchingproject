# Searching Project

Trabajo final de la materia Análisis Inteligente de Datos en entornos Big Data a cargo del profesor Dr. Olivas José Angel.

Searching Project, consiste en una aplicación web de búsqueda de contenido en un gran volúmen de información. En terminos generales
existe una base de datos con mas de 500 libros y desde una Web se pueden realizar búsquedas de contenido en toda esa base de datos 
, en donde también se puede acceder al libro para leerlo y navegarlo. :raised_hands:

## Comenzando

Por medio de estas instrucciones vas a poder: Clonar el proyecto, ejecutarlo y reproducirlo. :muscle:

Antes que nada quiero aclarar que yo estoy utilizando [Ubuntu](https://www.ubuntu.com/)  18.04 en una VirtualBox. :alien:

### Prerequisitos

Que cosas necesitas

* [Docker](https://docs.docker.com/install/linux/docker-ce/ubuntu/) - La web oficial de Docker (yo lo instalé en ubuntu)
* [Gitlab](https://about.gitlab.com/install/) - Web oficial de Gitlab
* :fire: 7 Gb de Ram asignado al sistema operativo (De no ser así en el despliegue posiblemente crasheen algunos contenedores)
* :fire: 4 nucleos de procesamiento asignado al sistema operativo (De no ser así en el despliegue posiblemente crasheen algunos contenedores)
* En el caso de utilizar un Ubuntu por medio de virtual box, hay que ejecutar esta linea de codigo por consola (ya que sino explota el   elasticsearch por falta de memoria): 
```
sudo sysctl -w vm.max_map_count=262144
```


### Instalación


Primero clonar el repo:

```
Via SSH (vas a tener que crear y agregar previamente el par de claves): 
Abrir consola -> git clone git@gitlab.com:ignaciomgu/searchingproject.git

o

Via HTTPS (directo): 
Abrir consola -> git clone https://gitlab.com/ignaciomgu/searchingproject.git
```

Una vez descargado el código ir dentro de la carpeta en donde se encuentra el prouecto:

Ejecutar primero:
```
Abrir consola -> docker swarm init
```
Se va a inicializar el swarm

Luego se deberá buildear la imagen del backend: 
```
Abrir consola -> docker build -t searching-backend:latest .
```

Y luego ejecutar:
```
Abrir consola -> docker stack deploy --compose-file docker-compose.yml searching_stack
```

Allí se creará una pila de ejecucion de servicios con todos los containers que se encuentran configurados en el docker-compose.yml

Para visualizar el estado de los mismos ejecutar:
```
Abrir consola -> docker stack ps searching_stack
```

O abrir un navegador para visualizarlo por interfaz grafica en [Visualizer](https://hub.docker.com/r/dockersamples/visualizer/tags/)
```
Abrir navegador-> 127.0.0.1:8090
```

No deberían estar tirando errores y deberian estár con estado en "Running".

Ahora se deberá ejecutar el script para cargar los indices en el elasticsearch, entonces:
```
Abrir consola -> docker container ls -a
```
Copiar el ID de Contenedor del que tiene nombre "searching-backend:latest" y ejecutar:
```
Abrir consola -> docker container exec -ti ID-CONTAINER /bin/bash
```

Aquí estaremos dentro del contenedor del backend, entonces ahora escribir en esa misma consola:
```
node server/elasticDBConectionManager.js
```

y luego:
```
node server/toProcessBooks.js
```

Aquí va a realizar la indexación y cuando esté todo listo se podrá ingresar a la web en "127.0.0.1:8080" :pray:


## Gif Demo

En este gif se encuentran todos estos pasos (realizados por mi localmente). También se pueden apreciar otras cosas como la utilización del sistema, etc.

* [Ir al Gif](https://gifyu.com/image/9v6q) - Se encuentra hosteado en  [Gifyu](https://gifyu.com) :cloud: por que es un archivo muy pesado.


## Contenido de carpetas y significado de los archivos mas importantes

* :file_folder: **Books** - Posee mas de 500 libros en formato ".txt" descargado de [Gutenberg Project](https://www.gutenberg.org/), que basicamente es una iniciativa pública en donde se suben :books: de todo tipo gratis en distintos formatos.

* :file_folder: **Public** - Posee el código de frontend hecho en [Vue JS](https://vuejs.org/), que al deployar con [Swarm](https://docs.docker.com/engine/swarm/) se depliega en un servidor web [NGINX](https://www.nginx.com/).

* :file_folder: **Server** - Posee el código del backend hecho en [Node JS](https://nodejs.org/es/) y los scripts en [Javascript](https://www.javascript.com/) de indexación de [Elasticsearch](https://hub.docker.com/_/elasticsearch).

* :flower_playing_cards: **demoSearchingProject.gif** - Gif con la demostración de instalación y reproducción.

* :flower_playing_cards: **diagrama.png** - Un diagrama explicando la arquitectura del sistema.

* :page_with_curl: **docker-compose.yml** - Configuración de despliegue de [Docker](https://www.docker.com/).

* :page_with_curl: **Dockerfile** - Crea una imagen [Docker](https://www.docker.com/) con el Backend del proyecto.

* :page_with_curl: **Informe** - Tiene como objetivo describir este trabajo, justificar las tecnologías utilizadas y detallar la experiencia de tanto las dificultades encontradas como de las soluciones integrales realizadas, y por último presentar una conclusión final. No obstante, en este informa no se encuentran agregadas las referencias bibliográficas, ya que fueron expuestas en este Readme.md por medio de los links agregados en todo el desarrollo.

## Arquitectura

<img src="https://gitlab.com/ignaciomgu/searchingproject/raw/master/diagrama.png">

En terimos generales, gracias a [Swarm](https://docs.docker.com/engine/swarm/) podemos desplegar dos replicas del backend desplegadas en [Node:Carbon](https://hub.docker.com/_/node/) balanceando la carga entre estos dos de las peticiones del cliente o frontend que se encuentran desplegadas a su vez en un [NGINX](https://www.nginx.com/).
Cada consulta al backend se recupera en caché [Redis](https://hub.docker.com/_/redis) y en caso de que no se encuentre en caché o se encuentre expirada, se realiza la consulta al [Elasticsearch](https://hub.docker.com/_/elasticsearch).


## Construido Con

* [Docker](https://www.docker.com/) - Para desarrollo y despliegue
* [Swarm](https://docs.docker.com/engine/swarm/) - Para desarrollo y despliegue
* [NGINX](https://www.nginx.com/) - Para desplegar el frontend
* [Node JS](https://nodejs.org/es/) - Para desarrollar el backend
* [Node:Carbon](https://hub.docker.com/_/node/)  - Para desplegar el backend (dos nodos)
* [Vue JS](https://vuejs.org/)  - Para desarrollar el frontend
* [Javascript](https://www.javascript.com/) - Para desarrollar el frontend y backend
* [Gutenberg Project](https://www.gutenberg.org/)  - Dataset de libros
* [Visualizer](https://hub.docker.com/r/dockersamples/visualizer/tags/)  - Para monitorear los contenedores
* [Elasticsearch](https://hub.docker.com/_/elasticsearch)  - Base de datos
* [Redis](https://hub.docker.com/_/redis)  - Caché
* [Gifyu](https://gifyu.com)  - Hosting del gif con la demo
* [Ubuntu](https://www.ubuntu.com/)  - Para realizar el desarrollo de la aplicación, pruebas y despliegue de Docker

## Versionado

Utilizando [GitLab](https://gitlab.com/ignaciomgu/searchingproject) para versionado. 

## Autor

* **Ignacio Gallardo** - [CV](https://ignaciogallardocv.herokuapp.com/)

