FROM node:carbon

WORKDIR /usr/src/app

COPY package*.json ./

RUN npm install

RUN npm install --save redis

COPY . .

CMD ["npm", "start"]
