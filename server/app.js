const redisClient = require('./redisDBController');
const Koa = require('koa')
const Router = require('koa-router')
const joi = require('joi')
const validate = require('koa-joi-validate')
const elasticController = require('./elasticDBController')
const app = new Koa()
const router = new Router()


// Se loguearán todas las requests
app.use(async (ctx, next) => {
  const start = Date.now()
  await next()
  const ms = Date.now() - start
  console.log(`${ctx.method} ${ctx.url} - ${ms}`)
})

// Se loguearán los errores
app.on('error', err => {
  console.error('Error interno del servidor', err)
})

// Se configura el permoiso para utilizar la api del back con clientes de navegadores
app.use(async (ctx, next) => {
  ctx.set('Access-Control-Allow-Origin', '*')
  return next()
})

/**
 * GET /buscar
 * Se busca un texto en toda la base de datos de libros
 * Query Params:
 * - textToSearch: String menor a 50 caracteres
 * - offset: Integer
 */
router.get('/buscar',
  validate({
    query: {
      textToSearch: joi.string().max(50).required(),
      offset: joi.number().integer().min(0).default(0)
    }
  }),
  async (ctx, next) => {
    const { textToSearch, offset } = ctx.request.query;
    const inCache = await redisClient.getAsync(textToSearch);
    if(JSON.parse(inCache)!==null){
	ctx.body = JSON.parse(inCache);
    }else{
	const result = await elasticController.searchText(textToSearch, offset);
        await redisClient.setAsync(textToSearch, JSON.stringify(result), 'EX', 3600); // 3600 segundos de time out de expiración
	ctx.body = result;
    }
  }
)

/**
 * GET /obtenerParrafo
 * Obtiene un párrafo determinado de un libro
 * Query Params:
 * - bookTitle: String menor a 200 caracteres
 * - start: Integer
 * - end: Integer
 */
router.get('/obtenerParrafo',
  validate({
    query: {
      bookTitle: joi.string().max(200).required(),
      start: joi.number().integer().min(0).default(0),
      end: joi.number().integer().greater(joi.ref('start')).default(10)
    }
  }),
  async (ctx, next) => {
    const { bookTitle, start, end } = ctx.request.query
    ctx.body = await elasticController.getBookParagraphs(bookTitle, start, end)
  }
)

const port = process.env.PORT || 3000
//App
app
  .use(router.routes())
  .use(router.allowedMethods())
  .listen(port, err => {
    if (err) console.error(err)
    console.log(`Backend escuchando en puerto ${port}`)
  })
