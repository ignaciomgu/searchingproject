const elasticsearch = require('elasticsearch')

// Configuración de Elsatic Search
const index = 'library'
const type = 'book'
const port = 9200
const host = 'elasticsearch'
const client = new elasticsearch.Client({ host: { host, port } })

// Chequea conexión con elasticsearch
async function checkConnection () {
  let isConnected = false
  while (!isConnected) {
    console.log('Conectando con Elastic Search')
    try {
      const health = await client.cluster.health({})
      console.log(health)
      isConnected = true
    } catch (err) {
      console.log('Concxión fallida, reintentando...', err)
    }
  }
}

// Borra indices y los resetea
async function resetIndex () {
  if (await client.indices.exists({ index })) {
    await client.indices.delete({ index })
  }

  await client.indices.create({ index })
  await putBookMapping()
}

// Agrega un libro a la base de datos
async function putBookMapping () {
  const schema = {
    title: { type: 'keyword' },
    author: { type: 'keyword' },
    location: { type: 'integer' },
    text: { type: 'text' }
  }

  return client.indices.putMapping({ index, type, body: { properties: schema } })
}

module.exports = {
  client, index, type, checkConnection, resetIndex
}
