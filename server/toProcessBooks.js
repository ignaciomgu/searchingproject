const fs = require('fs')
const path = require('path')
const esConnection = require('./elasticDBConectionManager')

// Por cada libro en formato ".txt" extrae el titulo, autor y contenido completo
function parseBookFile (filePath) {
  // Se lee el archivo ".txt"
  const book = fs.readFileSync(filePath, 'utf8')

  // Se encuentra el Titulo con expresiones regulares
  const title = book.match(/^Title:\s(.+)$/m)[1]
 // Se encuentra el Autor con expresiones regulares
  const authorMatch = book.match(/^Author:\s(.+)$/m)
  const author = (!authorMatch || authorMatch[1].trim() === '') ? 'Autor desconocido' : authorMatch[1]

  console.log(`Leyendo libro - ${title} de ${author}`)

  // Por medio de expresiones regulares se busca el inicio del contenido del libroc (con el objetivo de obtener todos los párrafos)
  const startOfBookMatch = book.match(/^\*{3}\s*START OF (THIS|THE) PROJECT GUTENBERG EBOOK.+\*{3}$/m)
  const startOfBookIndex = startOfBookMatch.index + startOfBookMatch[0].length
  // Por medio de expresiones regulares se busca el final del contenido del libro
  const endOfBookIndex = book.match(/^\*{3}\s*END OF (THIS|THE) PROJECT GUTENBERG EBOOK.+\*{3}$/m).index
  const paragraphs = book
    .slice(startOfBookIndex, endOfBookIndex) 
    .split(/\n\s+\n/g) 
    .map(line => line.replace(/\r\n/g, ' ').trim()) 
    .map(line => line.replace(/_/g, ''))
    .filter((line) => (line && line !== ''))

  console.log(`Parseando ${paragraphs.length} Parrafo\n`)
  return { title, author, paragraphs }
}

// Se almacenana los indices de los libros dentro de la base de datos Elastic Search
async function insertBookData (title, author, paragraphs) {
  let bulkOps = [] 
  for (let i = 0; i < paragraphs.length; i++) {
    bulkOps.push({ index: { _index: esConnection.index, _type: esConnection.type } })
    bulkOps.push({
      author,
      title,
      location: i,
      text: paragraphs[i]
    })
    if (i > 0 && i % 500 === 0) {
      await esConnection.client.bulk({ body: bulkOps })
      bulkOps = []
      console.log(`Parrafos Indexados ${i - 499} - ${i}`)
    }
  }
  await esConnection.client.bulk({ body: bulkOps })
  console.log(`Parrafos Indexados ${paragraphs.length - (bulkOps.length / 2)} - ${paragraphs.length}\n\n\n`)
}

/** Limpia los indices, y reindexa*/
async function readAndInsertBooks () {
  await esConnection.checkConnection()
  try {
    await esConnection.resetIndex()
    let files = fs.readdirSync('./books').filter(file => file.slice(-4) === '.txt')
    console.log(`${files.length} Archivos Encontrados`)
    for (let file of files) {
      console.log(`Leyendo Archivo - ${file}`)
      const filePath = path.join('./books', file)
      const { title, author, paragraphs } = parseBookFile(filePath)
      await insertBookData(title, author, paragraphs)
    }
  } catch (err) {
    console.error(err)
  }
}

readAndInsertBooks()
