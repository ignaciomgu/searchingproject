const { client, index, type } = require('./elasticDBConectionManager')

module.exports = {
  // Busca un texto en la base de datos de Eltastic
  searchText (textToSearch, offset = 0) {
    const body = {
      from: offset,
      query: { match: {
        text: {
          query: textToSearch,
          operator: 'and',
          fuzziness: 'auto'
        } } },
      highlight: { fields: { text: {} } }
    }

    return client.search({ index, type, body })
  },

  // Busca un párrafo dentro de un libro en la base de datos de Elasticsearch
  getBookParagraphs (titleOfTheBook, startIndex, endIndex) {
    const filter = [
      { term: { title: titleOfTheBook } },
      { range: { location: { gte: startIndex, lte: endIndex } } }
    ]

    const body = {
      size: endIndex - startIndex,
      sort: { location: 'asc' },
      query: { bool: { filter } }
    }

    return client.search({ index, type, body })
  }
}
